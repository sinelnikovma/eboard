package com.tsystems.eboard.backingbean;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;
import java.util.logging.Logger;

import javax.annotation.PostConstruct;
import javax.enterprise.context.ApplicationScoped;
import javax.inject.Inject;
import javax.inject.Named;

import com.tsystems.eboard.model.TreatmentEventDto;
import com.tsystems.eboard.model.TreatmentEventView;
import com.tsystems.eboard.restclient.RestClient;

@Named(value = "treatmentEvent")
@ApplicationScoped
public class TreatmentEventBackingBean implements Serializable {
	private static final long serialVersionUID = 1L;
	private static final Logger logger = Logger.getLogger(TreatmentEventBackingBean.class.getName());

//	private PushContext pushContext;
	@Inject
	private RestClient restClient;

	private List<TreatmentEventView> treatmentsList = new ArrayList<>();

	public TreatmentEventBackingBean() {
//		fillTable();
	}

	public void fillTable(List<TreatmentEventView> treatmentsList) {

	}

	@PostConstruct
	private void getTreatmentEvents() {
		List<TreatmentEventDto> allTreatmentEvent = restClient.getAllTreatmentEvent();
		List<TreatmentEventView> list = new ArrayList<>();

		if (allTreatmentEvent != null) {
			for (TreatmentEventDto treatmentEventDto : allTreatmentEvent) {

				TreatmentEventView view = new TreatmentEventView();
				view.setId(treatmentEventDto.getId());
				view.setPatient(treatmentEventDto.getPatient().getPatientFirstName() + " "
						+ treatmentEventDto.getPatient().getPatientLastName());
				view.setDrug(treatmentEventDto.getDrug().getDrugName());
				view.setDose(treatmentEventDto.getDose());
				view.setDate(treatmentEventDto.getDate());
				view.setTime(treatmentEventDto.getTime());

				list.add(view);
			}
		}

		this.setTreatmentsList(list);
	}

	/**
	 * @return the treatmentsList
	 */
	public List<TreatmentEventView> getTreatmentsList() {
		return treatmentsList;
	}

	/**
	 * @param treatmentsList the treatmentsList to set
	 */
	public void setTreatmentsList(List<TreatmentEventView> treatmentsList) {
		this.treatmentsList = treatmentsList;

		for (TreatmentEventView treatmentEventView : treatmentsList) {
			System.out.println(treatmentEventView);
		}
	}

}
