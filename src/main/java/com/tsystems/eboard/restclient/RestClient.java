package com.tsystems.eboard.restclient;

import java.util.List;

import javax.ejb.Local;

import com.tsystems.eboard.model.TreatmentEventDto;

@Local
public interface RestClient {

	public List<TreatmentEventDto> getAllTreatmentEvent();
}
