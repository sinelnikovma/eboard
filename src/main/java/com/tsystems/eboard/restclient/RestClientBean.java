package com.tsystems.eboard.restclient;

import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;

import javax.ejb.Stateless;
import javax.ws.rs.ProcessingException;
import javax.ws.rs.client.WebTarget;
import javax.ws.rs.core.GenericType;
import javax.ws.rs.core.Response;

import org.jboss.resteasy.client.jaxrs.ResteasyClient;
import org.jboss.resteasy.client.jaxrs.ResteasyClientBuilder;

import com.tsystems.eboard.model.TreatmentEventDto;

@Stateless
public class RestClientBean implements RestClient {

	private static final Logger logger = Logger.getLogger(RestClientBean.class.getName());

	@Override
	public List<TreatmentEventDto> getAllTreatmentEvent() {

		ResteasyClient client = new ResteasyClientBuilder().build();
		List<TreatmentEventDto> value = null;

		WebTarget target = client.target("http://localhost:8080/rcenter/api/all-treatment-event");
		Response response = null;

		try {
			response = target.request().get();
		} catch (ProcessingException e) {
			logger.log(Level.SEVERE, "target server 'http://localhost:8080/rcenter/api/all-treatment-event' is down",
					e.getMessage());
			return null;
		}

		if (response.getStatus() == 200) {
			value = response.readEntity(new GenericType<List<TreatmentEventDto>>() {
			});
		} else {
			logger.log(Level.INFO, "Connection failed");
		}

		response.close();

		return value;
	}
}
