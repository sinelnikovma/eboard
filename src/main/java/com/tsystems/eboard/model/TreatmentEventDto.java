package com.tsystems.eboard.model;

import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;

import com.fasterxml.jackson.annotation.JsonBackReference;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

@JsonIgnoreProperties(ignoreUnknown = true)
public class TreatmentEventDto {

	private Long id;
	@JsonBackReference(value = "patient")
	private PatientDto patient;

//	@JsonFormat(pattern = "yyyy-MM-dd'T'HH:mm")
	private LocalDateTime dateOfEvent;
	private String date;
	private String time;
	private String statusOfEvent;
	private String cancelReason;

	@JsonBackReference(value = "drug")
	private DrugDto drug;
	private String dose;

	/**
	 * @return the id
	 */
	public Long getId() {
		return id;
	}

	/**
	 * @param id the id to set
	 */
	public void setId(Long id) {
		this.id = id;
	}

	/**
	 * @return the patient
	 */
	public PatientDto getPatient() {
		return patient;
	}

	/**
	 * @param patient the patient to set
	 */
	public void setPatient(PatientDto patient) {
		this.patient = patient;
	}

	/**
	 * @return the dateOfEvent
	 */
	public LocalDateTime getDateOfEvent() {
		return dateOfEvent;
	}

	/**
	 * @param dateOfEvent the dateOfEvent to set
	 */
	public void setDateOfEvent(String dateOfEvent) {
		DateTimeFormatter formatter = DateTimeFormatter.ofPattern("yyyy-MM-dd'T'HH:mm");
		this.dateOfEvent = LocalDateTime.parse(dateOfEvent, formatter);
		System.out.println(dateOfEvent);
//		this.dateOfEvent = dateOfEvent;
	}

	/**
	 * @return the statusOfEvent
	 */
	public String getStatusOfEvent() {
		return statusOfEvent;
	}

	/**
	 * @param statusOfEvent the statusOfEvent to set
	 */
	public void setStatusOfEvent(String statusOfEvent) {
		this.statusOfEvent = statusOfEvent;
	}

	/**
	 * @return the cancelReason
	 */
	public String getCancelReason() {
		return cancelReason;
	}

	/**
	 * @param cancelReason the cancelReason to set
	 */
	public void setCancelReason(String cancelReason) {
		this.cancelReason = cancelReason;
	}

	/**
	 * @return the drug
	 */
	public DrugDto getDrug() {
		return drug;
	}

	/**
	 * @param drug the drug to set
	 */
	public void setDrug(DrugDto drug) {
		this.drug = drug;
	}

	/**
	 * @return the dose
	 */
	public String getDose() {
		return dose;
	}

	/**
	 * @param dose the dose to set
	 */
	public void setDose(String dose) {
		this.dose = dose;
	}

	/**
	 * @return the date
	 */
	public String getDate() {
		return date;
	}

	/**
	 * @param date the date to set
	 */
	public void setDate(String date) {
		this.date = date;
	}

	/**
	 * @return the time
	 */
	public String getTime() {
		return time;
	}

	/**
	 * @param time the time to set
	 */
	public void setTime(String time) {
		this.time = time;
	}

	/**
	 * @param dateOfEvent the dateOfEvent to set
	 */
	public void setDateOfEvent(LocalDateTime dateOfEvent) {
		this.dateOfEvent = dateOfEvent;
	}

}
