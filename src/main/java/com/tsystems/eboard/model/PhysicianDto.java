package com.tsystems.eboard.model;

import java.util.HashSet;
import java.util.Set;

import com.fasterxml.jackson.annotation.JsonManagedReference;

public class PhysicianDto {
	private Long id;
	private String physicianFirstName;
	private String physicianLastName;
	private String physicianCode;

	@JsonManagedReference
	private Set<PatientDto> patientList = new HashSet<>();
	private boolean isActive;

	/**
	 * @return the id
	 */
	public Long getId() {
		return id;
	}

	/**
	 * @param id the id to set
	 */
	public void setId(Long id) {
		this.id = id;
	}

	/**
	 * @return the physicianFirstName
	 */
	public String getPhysicianFirstName() {
		return physicianFirstName;
	}

	/**
	 * @param physicianFirstName the physicianFirstName to set
	 */
	public void setPhysicianFirstName(String physicianFirstName) {
		this.physicianFirstName = physicianFirstName;
	}

	/**
	 * @return the physicianLastName
	 */
	public String getPhysicianLastName() {
		return physicianLastName;
	}

	/**
	 * @param physicianLastName the physicianLastName to set
	 */
	public void setPhysicianLastName(String physicianLastName) {
		this.physicianLastName = physicianLastName;
	}

	/**
	 * @return the physicianCode
	 */
	public String getPhysicianCode() {
		return physicianCode;
	}

	/**
	 * @param physicianCode the physicianCode to set
	 */
	public void setPhysicianCode(String physicianCode) {
		this.physicianCode = physicianCode;
	}

	/**
	 * @return the patientList
	 */
	public Set<PatientDto> getPatientList() {
		return patientList;
	}

	/**
	 * @param patientList the patientList to set
	 */
	public void setPatientList(Set<PatientDto> patientList) {
		this.patientList = patientList;
	}

	/**
	 * @return the isActive
	 */
	public boolean isActive() {
		return isActive;
	}

	/**
	 * @param isActive the isActive to set
	 */
	public void setActive(boolean isActive) {
		this.isActive = isActive;
	}

}
