package com.tsystems.eboard.model;

public class DrugDto {

	private Long id;
	private String drugType;
	private String drugName;
	private boolean isActive;

	/**
	 * @return the id
	 */
	public Long getId() {
		return id;
	}

	/**
	 * @param id the id to set
	 */
	public void setId(Long id) {
		this.id = id;
	}

	/**
	 * @return the drugType
	 */
	public String getDrugType() {
		return drugType;
	}

	/**
	 * @param drugType the drugType to set
	 */
	public void setDrugType(String drugType) {
		this.drugType = drugType;
	}

	/**
	 * @return the drugName
	 */
	public String getDrugName() {
		return drugName;
	}

	/**
	 * @param drugName the drugName to set
	 */
	public void setDrugName(String drugName) {
		this.drugName = drugName;
	}

	/**
	 * @return the isActive
	 */
	public boolean isActive() {
		return isActive;
	}

	/**
	 * @param isActive the isActive to set
	 */
	public void setActive(boolean isActive) {
		this.isActive = isActive;
	}

}
