package com.tsystems.eboard.mdb;

import java.util.ArrayList;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;

import javax.ejb.ActivationConfigProperty;
import javax.ejb.MessageDriven;
import javax.inject.Inject;
import javax.jms.JMSException;
import javax.jms.Message;
import javax.jms.MessageListener;
import javax.jms.TextMessage;

import com.tsystems.eboard.backingbean.TreatmentEventBackingBean;
import com.tsystems.eboard.model.TreatmentEventDto;
import com.tsystems.eboard.model.TreatmentEventView;
import com.tsystems.eboard.restclient.RestClient;
import com.tsystems.eboard.websocket.SessionHandler;

@MessageDriven(name = "JmsReceiver", activationConfig = {
		@ActivationConfigProperty(propertyName = "destinationType", propertyValue = "javax.jms.Queue"),
		@ActivationConfigProperty(propertyName = "destination", propertyValue = "java:/jboss/exported/jms/RBoardQueue"),
		@ActivationConfigProperty(propertyName = "acknowledgeMode", propertyValue = "Auto-acknowledge") })
public class JmsReceiver implements MessageListener {

	private static final Logger logger = Logger.getLogger(JmsReceiver.class.getName());

	@Inject
	private RestClient restClient;

	@Inject
	private SessionHandler sessionHandler;

	@Inject
	private TreatmentEventBackingBean backingBean;

	public JmsReceiver() {

	}

	@Override
	public void onMessage(Message message) {
		TextMessage textMessage = null;

		if (message instanceof TextMessage) {
			textMessage = (TextMessage) message;

			try {
				logger.info(textMessage.getText());
			} catch (JMSException e) {
				logger.log(Level.SEVERE, e.getMessage());
			}

			List<TreatmentEventView> list = new ArrayList<>();
			getTreatmentEvent(list);

			backingBean.setTreatmentsList(list);
			sessionHandler.createAddMessage(list);
		}
	}

	private void getTreatmentEvent(List<TreatmentEventView> list) {
		List<TreatmentEventDto> allTreatmentEvent = restClient.getAllTreatmentEvent();

		for (TreatmentEventDto treatmentEventDto : allTreatmentEvent) {

			TreatmentEventView view = new TreatmentEventView();
			view.setId(treatmentEventDto.getId());
			view.setPatient(treatmentEventDto.getPatient().getPatientFirstName() + " "
					+ treatmentEventDto.getPatient().getPatientLastName());
			view.setDrug(treatmentEventDto.getDrug().getDrugName());
			view.setDose(treatmentEventDto.getDose());
			view.setDate(treatmentEventDto.getDate());
			view.setTime(treatmentEventDto.getTime());

			list.add(view);
		}
	}

}
