package com.tsystems.eboard.websocket;

import java.io.IOException;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;
import java.util.logging.Level;
import java.util.logging.Logger;

import javax.inject.Singleton;
import javax.json.JsonObject;
import javax.json.spi.JsonProvider;
import javax.websocket.Session;

import com.tsystems.eboard.model.TreatmentEventView;

@Singleton
public class SessionHandler {

	public static final Logger LOGGER = Logger.getLogger(SessionHandler.class.getName());

	private final Set<Session> sessions = new HashSet<>();
	private final List<TreatmentEventView> events = new ArrayList<>();

	public void addSession(Session session) {
		sessions.add(session);
	}

	public void removeSession(Session session) {
		sessions.remove(session);
	}

	public List<JsonObject> createAddMessage(List<TreatmentEventView> list) {

		List<JsonObject> jsonList = new ArrayList<>();
		JsonProvider provider = JsonProvider.provider();
		for (TreatmentEventView event : list) {
			JsonObject jsonObject = provider.createObjectBuilder().add("id", event.getId())
					.add("patient", event.getPatient()).add("drug", event.getDrug()).add("dose", event.getDose())
					.add("date", event.getDate()).add("time", event.getTime()).build();
			jsonList.add(jsonObject);
		}

		for (Session session : sessions) {
			sendToSession(session, jsonList);
		}
		return jsonList;
	}

	private void sendToSession(Session session, List<JsonObject> list) {
		try {
			session.getBasicRemote().sendText(list.toString());
		} catch (IOException e) {
			LOGGER.log(Level.SEVERE, "Result{0}", e.getMessage());
		}
	}
}
