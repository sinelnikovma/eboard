package com.tsystems.eboard.websocket;

import java.util.logging.Level;
import java.util.logging.Logger;

import javax.enterprise.context.ApplicationScoped;
import javax.inject.Inject;
import javax.websocket.OnClose;
import javax.websocket.OnError;
import javax.websocket.OnMessage;
import javax.websocket.OnOpen;
import javax.websocket.Session;
import javax.websocket.server.ServerEndpoint;

@ServerEndpoint("/events")
@ApplicationScoped
public class WebSocketServer {

	private static final Logger logger = Logger.getLogger(WebSocketServer.class.getName());

	@Inject
	SessionHandler sessionHandler;

	@OnOpen
	public void onOpen(Session session) {
		sessionHandler.addSession(session);
	}

	@OnClose
	public void onClose(Session session) {
		sessionHandler.removeSession(session);
	}

	@OnError
	public void onError(Throwable error) {
		logger.log(Level.SEVERE, null, error.getMessage());
	}

	@OnMessage
	public void onMessage(String message, Session session) {
		logger.log(Level.INFO, message);
	}

}
