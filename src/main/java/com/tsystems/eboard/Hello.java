package com.tsystems.eboard;

import java.io.IOException;
import java.util.logging.Level;
import java.util.logging.Logger;

import javax.ejb.EJB;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.tsystems.eboard.restclient.RestClient;

@WebServlet("/all")
public class Hello extends HttpServlet {

	private static final Logger logger = Logger.getLogger(Hello.class.getName());
	@EJB
	private RestClient restclient;

	public Hello() {
	}

	protected void doGet(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		restclient.getAllTreatmentEvent();
		response.getWriter().append("Served at: ").append(request.getContextPath());
	}

	protected void doPost(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		try {
			restclient.getAllTreatmentEvent();
		} catch (Exception e) {
			logger.log(Level.SEVERE, e.getMessage());
			throw new ServletException(e.getMessage());
		}

	}
}
