//window.onload = init;

var wsocket = new WebSocket("ws://localhost:8089/EBoard/events");
wsocket.onmessage = onMessage;
wsocket.onclose = onClose;

document.addEventListener("DOMContentLoaded", function(e) {
	window.addEventListener("load", connect, false);
	setTimeout('updateDateTime()', 1000);

}, false)

function connect() {
	wsocket = new WebSocket("ws://localhost:8089/EBoard/events");
	wsocket.onmessage = onMessage;
	wsocket.onclose = onClose;
	wsocket.onopen = onOpen;
}

function onMessage(event) {
	var list = JSON.parse(event.data);
	location.reload();
}

function onClose(event) {
	if (event.wasClean) {
		console.log("Connection was closed successfully");
	} else {
		console.log("Abnormal disconnection"); // например, "убит" процесс
												// сервера
	}
	console.log('Code: ' + event.code + ' reason: ' + event.reason);
}

function onOpen(event) {
	console.log("Connection is open");
}

function updateDateTime() {
	let currentDateTime = new Date();

	let inputTime = document.getElementById('time');
	let inputDate = document.getElementById('date');

	h = currentDateTime.getHours();
	if (h < 10)
		h = "0" + h;
	m = currentDateTime.getMinutes();
	if (m < 10)
		m = "0" + m;
	s = currentDateTime.getSeconds();
	if (s < 10)
		s = "0" + s;
	outString = h + ":" + m + ":" + s;
	inputTime.innerHTML = outString;
	outString = currentDateTime.getDate() + ".";
	outString += currentDateTime.getMonth() + ".";
	outString += currentDateTime.getFullYear();
	inputDate.innerHTML = outString;
	setTimeout("updateDateTime()", 1000);
}